const listInput = document.getElementById('listInput');
const sendInput = document.getElementById('sendInput');
const template = document.getElementById('list');
const listContainer = document.getElementById('listContainer');

console.log(listInput);
console.log(sendInput);

const addInput = () => {
    const item = template.content.cloneNode(true);
    console.log(item);
    if (listInput.value == '') {
        item.querySelector('label').textContent = `Empty task`;

    } else {
        item.querySelector('label').textContent = `${listInput.value}`;   
    }
    listContainer.appendChild(item);
    listInput.value = '';
    listInput.focus();
};

sendInput.addEventListener('click', addInput);